<?php
if (!function_exists('response')) {
    function response(array $data, int $status)
    {
        header('Content-Type: application/json; charset=utf-8');
        http_response_code($status);
        return json_encode($data);
    }
}

if (!function_exists('resource')) {
    function resource(string $keyword, array $arguments = []): string
    {
        $resource = include __DIR__ . '/resources.php';
        $message = vsprintf($resource[$keyword], $arguments);
        return $message;
    }
}
