<?php

use App\Framework\Router;
use App\Framework\Request;

require_once __DIR__ . '/vendor/autoload.php';

$router = new Router();
echo $router->route(new Request());
