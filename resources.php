<?php
return [
    'unique' => 'The %s has already been taken.',
    'phone' => 'The %s must be a valid phone number.',
    'max' => 'The %s must not be greater than %s characters.',
    'min' => 'The %s must be at least %s characters.',
    'required' => 'The %s field is required.',
];
