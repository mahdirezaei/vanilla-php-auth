<?php

namespace App\Framework;

use PDO;

class Database
{
    private static $instance;
    private static function connect(): PDO
    {
        $databaseConfig = Config::getInstance()->get('database');
        return new PDO(
            "mysql:host=$databaseConfig[host];dbname=$databaseConfig[name]",
            $databaseConfig['user'],
            $databaseConfig['password']
        );
    }
    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = self::connect();
        }
        return self::$instance;
    }
}
