<?php

namespace App\Framework;

use App\Services\Token;
use PDO;

class Authenticate
{

    private static $user;

    public static function check($header)
    {
        if (isset($header['Authorization'])) {
            $token = str_replace("Bearer ", "", $header['Authorization']);
            if ($id = Token::decode($token)) {
                $db = Database::getInstance();
                $query = 'SELECT * FROM users WHERE id = :id';
                $statement = $db->prepare($query);
                $statement->bindParam('id', $id, PDO::PARAM_STR);
                $statement->execute();
                if ($statement->rowCount() == 1) {
                    $user = $statement->fetch(PDO::FETCH_ASSOC);
                    self::setUser($user);
                    return true;
                }
            }
        }
        return false;
    }

    public static function getUser()
    {
        return [
            'id' => self::$user['id'],
            'name' => self::$user['name'],
            'phone' => self::$user['phone']
        ];
    }

    public static function setUser($user)
    {
        self::$user = (array)$user;
    }

    public static function login(array $credentials)
    {
        $db = Database::getInstance();
        $query = 'SELECT * FROM users WHERE phone = :phone';
        $statement = $db->prepare($query);
        $statement->bindParam('phone', $credentials['phone'], PDO::PARAM_STR);
        $statement->execute();
        $user = $statement->fetch(PDO::FETCH_ASSOC);
        if ($statement->rowCount() == 1 && !empty($user)) {
            if (password_verify($credentials['password'], $user['password'])) {
                return $user['id'];
            }
        }
    }

    public static function register(array $user)
    {
        $db = Database::getInstance();
        $query = "INSERT INTO users (`name`, `phone`, `password`) values(:name, :phone, :password)";
        $statement = $db->prepare($query);
        $statement->bindParam('phone', $user['phone'], PDO::PARAM_STR);
        $statement->bindValue('name', $user['name'], PDO::PARAM_STR);
        $statement->bindValue('password', password_hash($user['password'], PASSWORD_BCRYPT), PDO::PARAM_STR);
        $statement->execute();
        $user['id'] = $db->lastInsertId();
        if ($user['id']) {
            return $user['id'];
        }
    }
}
