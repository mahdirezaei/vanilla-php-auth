<?php

namespace App\Framework;

use App\Controllers\ErrorController;

class Router
{
    private $routeMap;

    private static $regexPatters = [
        'number' => '\d+',
        'string' => '\w'
    ];


    public function __construct()
    {
        $json = file_get_contents(
            __DIR__ . '/../../routes.json'
        );

        $this->routeMap = json_decode($json, true);
    }

    public function route(Request $request)
    {
        $path = '/' . $request->getParams()->getString('url');
        foreach ($this->routeMap as $route => $info) {
            if ($request->getMethod() === $info['type']) {
                $regexRoute = $this->getRegexRoute($route, $info);
                if (preg_match("@^/$regexRoute$@", $path)) {
                    return $this->executeController(
                        $route,
                        $path,
                        $info,
                        $request
                    );
                }
            }
        }
        $errorController = new ErrorController($request);
        return $errorController->notFound();
    }

    private function getRegexRoute(
        string $route,
        array $info
    ): string {
        if (isset($info['params'])) {
            foreach ($info['params'] as $name => $type) {
                $route = str_replace(
                    ':' . $name,
                    self::$regexPatters[$type],
                    $route
                );
            }
        }
        return $route;
    }

    private function extractParams(
        string $route,
        string $path
    ): array {
        $params = [];
        $pathParts = explode('/', $path);
        $routeParts = explode('/', $route);
        foreach ($routeParts as $key => $routePart) {
            if (strpos($routePart, ':') === 0) {
                $name = substr($routePart, 1);
                $params[$name] = $pathParts[$key + 1];
            }
        }
        return $params;
    }

    private function executeController(
        string $route,
        string $path,
        array $info,
        Request $request
    ) {
        $controllerName = '\App\Controllers\\' . $info['controller'];
        $controller = new $controllerName($request);

        if (isset($info['protected']) && $info['protected']) {
            if (!Authenticate::check(getallheaders()))
                return response(["message" => "Unauthorized !"], 401);
        }

        $params = $this->extractParams($route, $path);
        return call_user_func_array(
            [$controller, $info['method']],
            [$request, $params]
        );
    }
}
