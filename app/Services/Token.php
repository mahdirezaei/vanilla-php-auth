<?php

namespace App\Services;

use App\Framework\Config;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class Token
{

    public static function encode($data)
    {
        $key = Config::getInstance()->get('app')['key'];
        $payload = array(
            "iss" => $key,
            "iat" => time(),
            "nbf" => time(),
            "exp"  => time() + 3600,
            "data" => $data
        );

        return [
            'token' => JWT::encode(
                $payload,
                $key,
                'HS256'
            ),
            'expires_in' => $payload["exp"]
        ];
    }

    public static function decode(string $jwt)
    {
        try {
            return JWT::decode(
                $jwt,
                new Key(
                    Config::getInstance()->get('app')['key'],
                    'HS256'
                )
            )->data;
        } catch (\Exception $e) {
        }
    }
}
