<?php

namespace App\Rules;

interface Rule
{
    public function passes($value): bool;
    public function message($attribute): string;
}
