<?php

namespace App\Rules;

class Max implements Rule
{
    protected $max;

    public function __construct($max)
    {
        $this->max = $max;
    }

    public function passes($value): bool
    {
        return (strlen($value) <= $this->max);
    }

    public function message($attribute): string
    {
        return resource('max', [$attribute, $this->max]);
    }
}
