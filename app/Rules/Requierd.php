<?php

namespace App\Rules;

class Requierd implements Rule
{
    public function passes($value): bool
    {
        return (bool)(isset($value) && trim($value) !== '');
    }

    public function message($attribute): string
    {
        return resource('required', [$attribute]);
    }
}
