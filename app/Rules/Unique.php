<?php

namespace App\Rules;

use App\Exceptions\NotFoundException;
use App\Framework\Database;
use PDO;

class Unique implements Rule
{
    private $table;
    private $column;

    public function __construct($table, $column)
    {
        $this->table = $table;
        $this->column = $column;
    }

    public function passes($value): bool
    {
        $db = Database::getInstance();
        $query = "SELECT COUNT(*) FROM $this->table WHERE $this->column = :value";
        $statement = $db->prepare($query);
        $statement->bindParam('value', $value, PDO::PARAM_STR);
        $statement->execute();
        if ($statement->rowCount() == 1) {
            return false;
        }
        return true;
    }

    public function message($attribute): string
    {
        return resource('unique', [$attribute]);
    }
}
