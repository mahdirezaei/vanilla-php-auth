<?php

namespace App\Rules;

class Min implements Rule
{
    protected $min;

    public function __construct($min)
    {
        $this->min = $min;
    }

    public function passes($value): bool
    {
        return (strlen($value) >= $this->min);
    }

    public function message($attribute): string
    {
        return resource('min', [$attribute, $this->min]);
    }
}
