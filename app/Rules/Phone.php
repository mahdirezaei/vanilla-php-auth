<?php

namespace App\Rules;

class Phone implements Rule
{
    public function passes($value): bool
    {
        return (bool)preg_match('/^09((1|3)\d|(9|2)[0-2]|0[1-5])\d{7}$/', $value);
    }

    public function message($attribute): string
    {
        return resource('phone', [$attribute]);
    }
}
