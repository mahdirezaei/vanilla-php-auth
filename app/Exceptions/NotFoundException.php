<?php

namespace App\Exceptions;

use Exception;

class NotFoundException extends Exception
{
    public function __construct($message = null)
    {
        $message = $message ?: 'Nothing found.';
        parent::__construct($message);
    }
}
