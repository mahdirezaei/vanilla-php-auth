<?php

namespace App\Exceptions;

use Exception;

class DatabaseException extends Exception
{
    public function __construct($message = null)
    {
        $message = $message ?: 'An error occurred while connecting to the database.';
        parent::__construct($message);
    }
}
