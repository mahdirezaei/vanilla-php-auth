<?php

namespace App\Controllers;

use App\Framework\Config;
use App\Framework\Request;
use App\Framework\Database;

abstract class Controller
{
    protected $request;
    protected $config;
    protected $db;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->db = Database::getInstance();
        $this->config = Config::getInstance();
    }
}
