<?php

namespace App\Controllers;

use App\Framework\Request;
use App\Framework\Authenticate;

class HomeController extends Controller
{

    public function index(Request $request)
    {
        return response(Authenticate::getUser(), 200);
    }
}
