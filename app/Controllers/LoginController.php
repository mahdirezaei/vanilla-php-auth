<?php

namespace App\Controllers;

use App\Framework\Authenticate;
use App\Framework\Request;
use App\Rules\Min;
use App\Rules\Phone;
use App\Rules\Requierd;
use App\Services\Token;
use PDO;


class LoginController extends Controller
{

    public function login(Request $request)
    {
        $validate = $request->validate([
            'phone' => [new Requierd(), new Phone()],
            'password' => [new Requierd(), new Min(6)]
        ]);

        if ($validate !== true) {
            return response($validate, 400);
        }

        $credentials = [
            "phone" => $request->getParams()->getString('phone'),
            "password" => $request->getParams()->getString('password')
        ];

        $authenticate = Authenticate::login($credentials);
        if (is_null($authenticate)) {
            return response(["message" => "Bad Request!"], 400);
        }

        return response(Token::encode($authenticate), 201);
    }
}
