<?php

namespace App\Controllers;

class ErrorController extends Controller
{
    public function notFound()
    {
        return response(["message" => "Not Found!"], 404);
    }
}
