<?php

namespace App\Controllers;

use App\Framework\Request;
use App\Rules\Unique;
use App\Rules\Min;
use App\Rules\Phone;
use App\Rules\Requierd;
use App\Framework\Authenticate;
use App\Services\Token;

class RegisterController extends Controller
{


    public function register(Request $request)
    {
        $validate = $request->validate([
            'phone' => [new Requierd(), new Phone(), new Unique('users', 'phone')],
            'password' => [new Requierd(), new Min(6)]
        ]);

        if ($validate !== true) {
            return response($validate, 400);
        }

        $user = [
            'name' => $request->getParams()->getString('name'),
            'password' => $request->getParams()->getString('password'),
            'phone' => $request->getParams()->getString('phone')
        ];

        $authenticate = Authenticate::register($user);
        if (is_null($authenticate)) {
            return response(["message" => "Bad Request!"], 400);
        }


        return response(Token::encode($authenticate), 201);
    }
}
